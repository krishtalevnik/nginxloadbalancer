import datetime
import json
import sys
import time
import os
import traceback
from backports import zoneinfo
import requests


# Class for working with oncall API
class Oncall:
    def __init__(self, api_url):
        self.api_url = api_url

        self.session = requests.session()

    def logout(self):
        url = "http://" + os.environ['API_URL'] + "/logout"
        self.raw(method='POST', url=url)

    def get_auth_data(self, login, password):
        url = "http://" + os.environ['API_URL'] + "/login"
        data = {
            "username": login,
            "password": password
        }
        headers = {
            "Content-Type": "application/x-www-form-urlencoded"
        }
        response = self.raw(method='POST', url=url, data=data, headers=headers)

        if response is None:
            return None

        response_body = response.json()
        response_headers = response.headers

        cookies = response_headers['Set-cookie']
        start = 'oncall-auth='
        end = ';'
        onauth_cookie = cookies[cookies.find(start) + len(start):cookies.find(end)]

        return {
            "cookies": {
                "oncall-auth": onauth_cookie
            },
            "headers": {
                'x-csrf-token': response_body["csrf_token"]
            }
        }

    def raw(self, method, uri='', params=None, data=None, headers=None, cookies=None, url=None):
        if url is None:
            url = self.api_url + uri

        if headers is None:
            headers = {}

        if "Content-Type" not in headers:
            headers["Content-Type"] = 'application/json'

        if headers["Content-Type"] == 'application/json':
            headers["Accept"] = 'text/plain'
            data = json.dumps(data)

        try:
            response = self.session.request(
                method,
                url,
                headers=headers,
                params=params,
                data=data,
                cookies=cookies
            )
            response.raise_for_status()
        except requests.exceptions.HTTPError as err:
            print(err)
            return None

        return response

    def find(self, model, id_value, id_field):
        params = {id_field: id_value}
        records = self.raw('GET', '/' + model, params=params).json()

        if len(records):
            return records[0]

        return None

    def find_by_id(self, model, id_value):
        records = self.raw('GET', '/' + model + '/' + id_value).json()

        if len(records):
            return records

        return None

    def create_record_if_not_exists(self, model, data, id_fields, cookies=None, headers=None) -> bool:
        params = {}

        for id_field in id_fields:
            if id_field in data:
                params[id_field] = data[id_field]

        records = self.raw('GET', '/' + model, data=data, params=params, cookies=cookies, headers=headers).json()

        if (len(params) != 0) & len(records):
            return False

        return self.create_record(model, data=data, cookies=cookies, headers=headers)

    def create_record(self, model, data, cookies=None, headers=None) -> bool:
        if self.raw('POST', '/' + model, data=data, cookies=cookies, headers=headers) is None:
            return False

        return True


def parse_date_to_unix(date, timezone='UTC'):
    raw_date = date.split('/')
    raw_date = list(map(lambda x: int(x), raw_date))
    tz_info = zoneinfo.ZoneInfo(key=timezone)
    date_in_datetime = datetime.datetime(raw_date[2], raw_date[1], raw_date[0], tzinfo=tz_info)
    unix_time = time.mktime(date_in_datetime.timetuple())

    return unix_time


class DataProvider:
    def __init__(self, folder):
        self.folder = folder

        self.session = requests.session()

    def get_data_from_json(self, filename):
        f = open(self.folder + '/' + filename)
        return json.load(f)

    def get_users(self):
        return self.get_data_from_json('users.json')['users']

    def get_teams(self):
        return self.get_data_from_json('teams.json')['teams']

    def get_rosters(self):
        return self.get_data_from_json('roasters.json')['roasters']

    def get_events(self):
        return self.get_data_from_json('events.json')['events']


client = Oncall("http://" + os.environ['API_URL'] + "/api/v0")


def create_user(user):
    if client.create_record_if_not_exists('users', user, ['name']):
        client.raw(method='PUT', uri='/users/' + user['name'], data=user)
        print('User ' + user['name'] + ' is created')
    else:
        print('User ' + user['name'] + ' is not created')


def create_event(event):
    team = client.find_by_id('teams', event['team'])
    team_timezone = team['scheduling_timezone']

    event['start'] = parse_date_to_unix(event['start'], team_timezone)
    event['end'] = parse_date_to_unix(event['end'], team_timezone)

    if client.create_record('events', event):
        print('Event is created')
    else:
        print('Event is not created')


def create_roster(roster):
    team = client.find_by_id('teams', roster['team_name'])

    if team is None:
        print('Team with name ' + roster['team_name'] + ' not found!')
        return

    uri = 'teams/' + str(team['name']) + '/rosters'
    rosters = client.raw(uri='/' + uri, method='GET').json()

    if roster['name'] in rosters:
        print('Roster ' + roster['name'] + ' already exists')
        return

    if (client.create_record(uri, data=roster)) is False:
        print('Roster ' + roster['name'] + ' is not created')
        return
    else:
        print('Roster ' + roster['name'] + ' is created')

    users_uri = uri + '/' + roster['name'] + "/users"

    for user in roster['users']:
        if client.create_record(users_uri, data={"name": user}) is False:
            print('Roster user creation failed')
            continue


def create_team(team):
    user = client.find('users', team['admin_name'], 'name')

    if user is None:
        print('User with name ' + team['admin_name'] + ' not found!')
        return

    auth_data = client.get_auth_data(user['name'], 'any')
    if auth_data is None:
        print('Error on authorization')
        return

    if client.create_record_if_not_exists(
            'teams',
            team,
            ['name'],
            headers=auth_data['headers'],
            cookies=auth_data['cookies']
    ):
        print('Team ' + team['name'] + ' is created')
    else:
        print('Team ' + team['name'] + ' is not created')

    client.logout()


def wait_for_oncall():
    tries = 0
    while True:
        try:
            client.raw('GET', '/teams')
            break
        except Exception as e:
            if tries > 20:
                print('Waited too long for API server to come up. Bailing.')
                sys.exit(1)

            if 'DEBUG' in os.environ:
                if os.environ['DEBUG']:
                    print(traceback.format_exc())

            time.sleep(2)
            tries += 1
            continue


def main():
    print('Initialize seeder')
    wait_for_oncall()
    data = DataProvider('./seeders')

    seeder_users = data.get_users()
    for user in seeder_users:
        create_user(user)

    seeder_teams = data.get_teams()
    for team in seeder_teams:
        create_team(team)

    seeder_rosters = data.get_rosters()
    for roster in seeder_rosters:
        create_roster(roster)

    seeder_events = data.get_events()
    for event in seeder_events:
        create_event(event)

    print('Initialized seeder')


if __name__ == '__main__':
    main()
